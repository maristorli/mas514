.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway<br>Kennet Karlsen, Silje Wetrhus Hebnes, Mari Storli</p>
   

|

.. image:: ./figs/jetbot.jpg
   :width: 12cm
   :align: center

|
|

MAS514 - Robotics and Instrumentation
===================================================

Lecturer
---------

- Daniel Hagen, PhD
   - Associate Professor
   - https://www.uia.no/en/kk/profile/danielh

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   src/Intro
   src/Transform
   src/Odometry
   src/Laserscan
   src/MotorController
   src/Navigation
   src/Repository
   src/Run
   src/FutureWork
 


.. Indices and tables
.. ==================



.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
