Navigation Stack
================

To set up the navigation stack this tutorial was used: http://wiki.ros.org/navigation/Tutorials/RobotSetup
The navigation stack is a rospackage that can be used for autonomous navigation of a robot.
By providing sensor data, odometry, transforms, base controller and goal pose,
the navigation stack can localize and plan trajectories for the robot. 
The figure below presents an overview of the different parts from the navigation package.

.. image:: ../figs/navigation.png
   :width: 12cm
   :align: center

By going through the tutorial linked above, the following steps were done:
A package was created with this command:

* ``catkin_create_pkg jetbot_nav mas514 realsense2_camera pointcloud_to_laserscan``.

Next, a robot configuration launch file was created. The content of this file was
moved to the launch file for the mas514 package, ``start_project.launch`` to 
minimize the number of launch files. These lines were added:

.. literalinclude:: ../code/robot_config.launch
    :language: c++ 

Further, configurations for the costmaps were created. 
The ``costmap_common_params.yaml`` were created with the content given the tutorial. This file
contains the configurations that are common for both global and local costmap.
The footprint were set with clockwise specificatins. The robots size was measured and
some additional cm were added to have some safety margin. The inflation radius was 
set to 0.2m. Laserscan was set as the observation source and the laserscan sensor was defined
in line 9.

The configurations for the global costmap were set in the file ``global_costmap_params.yaml``
The global frame, robot base frame, and update frequency were set as default. Static map was
set to fault since we are not using a map.

The configurations for the local costmap were set in the file ``local_costmap_params.yaml``
This file contains the same configuration as ``global_costmap_params`` in addition to some extra.
All parameters were set to the same value as in the tutorial.

The ``base_local_planner_params.yaml`` file was created and contains the velocity and acceleration
restrictions for the robot. These parameters were reduced to slow down the robot while testing. 

Finially, a ``move_base.launch`` file was created. ``amcl_omni.launch`` was replaced with 
``amcl_diff.launch`` in line 7. Additionally, the package name was change to match the 
name of the package that we created, ``jetbot_nav``.

