Project Description
===================


.. figure:: ../figs/jetbot.jpg
   :width: 90%
   :align: center


Project Description
-------------------
This project is a part of the course MAS514 Robotics and instrumentation, which is a course in the program for the Mechatronic master at the University of Agder. This report contains a combination of using different elements such as see, think, and act. Motion control, perception, localization and mapping and motion planning are going to be implemented on a JetBot by using ROS Navigation to demonstrate autonomous operations. From this report, the reader should be able to implement the navigation stack on a jetbot by cloning the repository and run the commands given in the Run chapter.  


Initial System
--------------
Before starting the project, the git: https://gitlab.com/hagenmek/mas514
was forked. This git contains the ROS package "mas514" which has some
already working nodes included. This package was used as a base when setting 
up the jetbot to run the navigation stack.  






.. |br| raw:: html

      <br>
