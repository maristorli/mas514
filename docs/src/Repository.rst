Repository
================
This chapter explains the content of the git repository.
The catkin_ws contains all packages and nodes used for this project.
All packages are located in the ``catkin_ws/src`` folder. The following packages are used:

* mas514: Contains the launch file for the project ( ``start_project.launch`` ), odometry publisher node ( ``odometry_pub.py`` ), and base controller to drive the jetbot ( ``Maincontroller.py`` and ``JetbotController.py`` )

* realsense-ros: contains the driver for the LiDAR camera. The launch file ``rs_camera.launch`` from the package ``realsense2_camera`` is used. 

* pointcloud_to_laserscan: package to convert the pointcloud from the LiDAR to 2D laserscan. The content from the ``sample_node.launch`` was copied and pasted into ``start_project.launch``

* navigation: the navigation package.

* localization_data_pub: package created for setting initial pose and goal in rviz.

* slam_gmapping: package used to create a map that can be used for the navigation stack.

Additional files within the repository are used for building the report. That includes both reStructuredText files, figures, and code files (used to include relevant code in the report).