Motor Controller
================
The navigation stack publishes velocity commands using geometry_msgs/Twist message.
Hence, a node that subscribes to this message is required. This node is the
``Maincontroller.py`` file. The node takes the velocity commands that are given in 
x, y, z, roll, pitch, yaw, and calculates the required ticks for the servomotors to achieve the 
subscribed velocities. The ticks are published as ``/servoSetpoints`` such that another node, ``JetbotController.py``
can subscribe to the ticks and send the pwm signal to the motors.
No changes were made in ``JetbotController.py``. The node ``Maincontroller.py`` can be found in 
the repository ``/src/mas514/src/Maincontroller.py``

----------------
Keyboard Control
----------------
The jetbot can be controlled using a keybord. This is done by using a ROS package called 
teleop_twist_keyboard. Documentation is available here: http://wiki.ros.org/teleop_twist_keyboard.
To use this package, is was installed using sudo apt-get:

* ``sudo apt-get install ros-melodic-teleop-twist-keyboard``

The package comes with a node that publishes the same velocity commands as the navigation
stack, based on input from the keyboard. This node can be run with this command:

* ``rosrun teleop_twist_keyboard teleop_twist_keyboard.py``




