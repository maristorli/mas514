Transform
=========

Different frames are defined such that sensor data can be 
transformed from one frame to another. This is required as
the wheels are located within one frame and the LiDAR is
located in another frame. The ROS package tf is used to 
do this transformation. More information about tf can be
found here: http://wiki.ros.org/tf. The frames are defined
as in this guide: https://automaticaddison.com/coordinate-frames-and-transforms-for-ros-based-mobile-robots/.
However, only encoder data and no IMU are used to publish the odometry.

The frames are defined in the figure presented below and the
frames are defined as the following:

* map: an arbitrary fixed position in the world
* odom: fixed frame at the initial position of the jetbot. The transformation between map and odom is set to be zero for all translations and rotations meaning that the frames are equal.
* base_footprint: at ground level at the center of the jetbot. This frame depends on the position of the jetbot, and taken care of within the odometry_pub node (see chapter Odometry).
* base_link: in the center of the jetbot. 
* base_laser: the center of the LiDAR. 


.. figure:: ../figs/frames.png
   :width: 90%
   :align: center


In the launch file for the ROS package mas514, these lines are added:

.. literalinclude:: ../code/tf.cpp
    :language: c++ 

where the base_link frame is set 6cm above (+6cm in z) the base_footprint
frame and the laser frame is set to 9cm in x and 3cm in z offset
from base_link. These are all the static transforms and the offsets are 
measured roughly. Hence, some unacurracy are expected. The dynamic 
transform between odom and base_footprint are implemented in the 
odometry_pub.py node, which is showed below.


By running the following command:

* ``rosrun tf view_frames``

an overview of all the transforms are created as a pdf, see overview below.
The pdf can also be found in the repository in the folder docs/figs.

.. figure:: ../figs/frames_tf.PNG
   :width: 90%
   :align: center

