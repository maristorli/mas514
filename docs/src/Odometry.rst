Odometry Publisher
==================
The navigation stack requires odometry data to determine the
position of the jetbot. The odometry data must be published 
using tf and the message nav_msgs/Odometry. The odometry is 
calculated based on motor encoders. The encoders are connected
to an Arduino nano which is connected to the Jetson nano. For
the wiring see the figure below. The odometry part was done by
following these guides:

* https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-publish-wheel-encoder-data-using-ros-and-arduino
* https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-publish-odometry-using-wheel-encoder-position

.. image:: ../figs/Arduino_encoder_wiring.png
   :width: 12cm
   :align: center

These steps requires that Arduino IDE is installed on the
jetson nano. Arduino IDE can be downloaded here: https://downloads.arduino.cc/arduino-1.8.16-linuxaarch64.tar.xz
and installed by running the install.sh file. This can be
done by navigating through the unpacked folder by using
the file explorer or by giving commands in a terminal.
In case the file is not executable, this can be done by
right-clicking on the file and go to properties. Under
the tab permissions, the "Allow executing file as program" 
must be enabled. Additionally, serial permission was enabled.
This was done by the following command in a terminal:

* ``sudo usermod -a -G dialout jetbot``

A reboot of the jetbot is required to initiate the new
permission.

When Arduino IDE was installed, an arduino script was 
uploaded to the nano. This script reads the number of turns
of each motor and calculates the rotational angle which are
published as a ros topic. See the arduino code below:

.. literalinclude:: ../code/Encoder.ino
    :language: c++ 

In order for the arduino to be able to publish topics on ROS,
the package rosserial_arduino was used. The following commands
were used to implement this package.

* ``sudo apt-get install ros-melodic-rosserial-arduino``
* ``sudo apt-get install ros-melodic-rosserial``

Instals the package rosserial_arduino.

* ``cd Arduino/libraries``

Navigates to the libraries folder.

* ``rm -rf ros_lib``

Removes the file ros_lib

* ``rosrun rosserial_arduino make_libraries.py .``

Runs the node make_libraries.py in the rosserial_arduino package.

The odometry needs to be published in a nav_msgs/Odometry message.
Hence, a python node that subscribes to the angles published by 
the arduino script and publishes the position and velocity is created.
The node can be found in the repository: /catkin_ws/src/mas514/src/odometry_pub.py.
