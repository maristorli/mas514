Run the navigation stack
========================
The following instructions assumes that all harware components are working and set up.
Clone the catkin repository from the git with this command:

* ``git clone xxx``

Then, navigate to the catkin workspace and build with this command:

* ``cd catkin_ws``
* ``catkin_make``


To start the navigation stack execute the following commands.

* ``roscore``
* ``roslaunch realsense2_camera rs_camera.launch``
* ``roslaunch mas514 start_project.launch``
* ``roslaunch jetbot_nav move_base.launch``
* ``rviz``

In rviz, open the rviz setup file located in ``catkin_ws``



--------------
Remote Control
--------------
An external laptop was used to remotly control the jetbot and 
visualize the navigation in rviz. This laptop is set as a rosmaster
and the jetson is set as a ros host. To set up the master and host, 
step 3.1.6 and 3.2.6 (7-10) from https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup was used. 

These steps were used to set up the laptop as master:

* ``nano ~/.bashrc``
Opens the bashrc file. The following lines were added to the buttom 
of the file:

.. literalinclude:: ../code/master.cpp
    :language: c++ 

Saved the file with ctrl+S and exited the editor with ctrl+X. Then the 
this command was executed:

* ``source ~/.bashrc``

The same steps were used for setting up the host, but this lines were
added to the bashrc file:

.. literalinclude:: ../code/host.cpp
    :language: c++ 

The ip adresses can be found with this command:

* ``ifconfig``


Additionally, ssh was set up in visual studio code. The extension Remote-SSH was installed.
The connection to the jetson nano was esablished by using the hostname: "jetbot@10.42.0.69"
The password "jetbot" was given when prompted to do so.
The laptop was connected to internet through ethernet and the jetson was connected to 
a hotspot that the laptop shared. With successful ssh connection, the local files should
be accessable in visual stuido code. Additionally, the jetson can be operated through 
terminals in visual studio code.