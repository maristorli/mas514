Future Work
===========
This chapter contains descriptions of parts that were started but still remains to be completed. 

---
Map
---
The navigation stack can take a map as input. The map can be created by recording laserscan data while driving the robot
around. The package gmapping and map_saver can be used for this. This tutorial can be followed to create the map: http://wiki.ros.org/slam_gmapping/Tutorials/MappingFromLoggedData
If a map is created and will be used, the parameters in the static_map in ``global_costmap_params.yaml`` must be set to true.

-------------
goal position
-------------
An attempt of creating a package that contains nodes for setting a goal position in rviz was done. 
This guide was followed: https://automaticaddison.com/how-to-create-an-initial-pose-and-goal-publisher-in-ros/

------------------------------
Tuning of the navigation stack
------------------------------
More accurate measurements of the jetbot's dimensions should be taken in order to optimise the parameters defined in the ``jetbot_nav``
package and in the transforms defined in ``start_project.launch`` . Additionally, tuning of parameters for the local and global costmaps
might need some tuning too. These were set according to the tutorial and should be tested and tuned to increase the performace of the 
navigation stack.  