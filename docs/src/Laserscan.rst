Laserscan
=========

The navigation stack requires sensor data from the environment.
This data is used to avoid obstacles and should be published as 
sensor_msgs/LaserScan or sensor_msgs/PointCloud messages. In this
project, we have used a Intel realsense L515 LiDAR to capture
this data. A ROS driver for the LiDAR is used to publish a pointcloud.
A ROS package, pointcloud_to_laserscan, is used to convert the
pointcloud into 2D laserscan. 

First, the ROS driver for the LiDAR was installed following
the step provided in the guide: https://hagenmek.gitlab.io/mas514/src/guides.html#install-realsense2-ros
The ROS package was downloaded form this git: https://github.com/IntelRealSense/realsense-ros.git
and the server's public key is availabel here: https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_jetson.md

    * ``sudo apt-get update``
    * ``sudo apt-get upgrade``

Updates and upgrades packages.

    * ``sudo apt-get install ros-$ROS_DISTRO-realsense2-camera``
    * ``sudo apt-get install ros-$ROS_DISTRO-realsense2-description``

Installes ROS distributions.

    * ``sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE``

Registers the server's public key.

    * ``sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u``

Adds the server to the list of repositories.

    * ``sudo apt-get install librealsense2-utils``
    * ``sudo apt-get install librealsense2-dev``

Installs the software development kit (SDK).

    * ``cd catkin_ws``
    * ``cd src``

Navigates to the workspace source folder.

    * ``git clone https://github.com/IntelRealSense/realsense-ros.git``
    * ``cd realsense-ros/``
    * ``git checkout `git tag | sort -V | grep -P "^2.\d+\.\d+" | tail -1```

Downloads the git repository into the workspace source folder.

    * ``cd ..``
    * ``catkin_init_workspace``
    * ``cd ..``
    * ``catkin_make clean``

Navigates back to the catkin workspace, initiates and builds packages.

    * ``catkin_make -DCATKIN_ENABLE_TESTING=False -DCMAKE_BUILD_TYPE=Release -DBUILD_WITH_CUDA=true``

    * ``echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc``
    * ``source ~/.bashrc``


To convert the pointcloud to laserscan, the ROS package
pointcloud_to_laserscan was downloaded and built with the 
following commands:

    * ``sudo apt-get install ros-melodic-pointcloud-to-laserscan``

Installs the package.

    * ``cd catkin_ws/src``
    * ``git clone -b lunar-devel https://github.com/ros-perception/pointcloud_to_laserscan``

Clones git repository to the catkin workspace source folder.

    * ``cd ..``
    * ``catkin_make``

Navigates back to the catkin workspace and builds the packages.

The launch file ``rs_camera.launch`` was used to launch the driver for the camera.
However, some changes were made:

* Line 46: enable_pointcloud was set to true
* Line 47: pointcloud_texture_stream was set to RS2_STREAM_ANY


