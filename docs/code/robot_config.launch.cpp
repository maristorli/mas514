
<!--- Run AMCL --> 
    <include file="$(find amcl)/examples/amcl_diff.launch" />

    <node pkg="move_base" type="move_base" respawn="false" name="move_base" output="screen">
    <rosparam file="$(find jetbot_nav)/costmap_common_params.yaml" command="load" ns="global_costmap" /> 
    <rosparam file="$(find jetbot_nav)/costmap_common_params.yaml" command="load" ns="local_costmap" />
    <rosparam file="$(find jetbot_nav)/local_costmap_params.yaml" command="load" />
    <rosparam file="$(find jetbot_nav)/global_costmap_params.yaml" command="load" /> 
    <rosparam file="$(find jetbot_nav)/base_local_planner_params.yaml" command="load" />
 </node>