For å laste opp endringer
    1) ctrl + s                     %% lagrer endringer på pc
    2) git add -A
    3) git commit -m "melding"
    4) git push                     %% laster opp endringer

For å oppdatere filer på pc
    1) cd <lokal adresse til fil>   %% for å gå til mappen hvor filen ligger
    2) git pull                     %% laster ned fra git

    
